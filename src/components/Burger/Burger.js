import React from 'react';
import Burgeringredent from './Burgeringredent/Burgeringredent';
import classes from './Burger.module.css';

 const  Burger = (props) => {
     //const transingredent = Object.key(props.ingredent);
     //console.log(transingredent);
    return (
        <div className={classes.Burger}>
            <Burgeringredent type="bread-top"/>
            <Burgeringredent type="bread-bottom"/>
        </div>
        
    )
}

export default Burger;
