import React, { Component } from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';


class BurgerBuilder extends Component {
    // constructor(props){
    //     super()
    // }
    state = {
        ingredent : {
            salad:1,
            bacon:1,
            cheese:2,
            meat:2
        }
    }

    render() {
        return (
            <Aux>
                <Burger ingredent={this.state.ingredent}/>
                <div>Burger control</div>
            </Aux>
        )
    }
}

export default BurgerBuilder;